/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab2;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ahmed
 */
public class LabTest {
   @Test
   public void givenMonthlyEmployee_whenRequestSalary_thenWillReturned(){
       //Given
       Employee employee=new MonthlyEmployee("Employee",2);
       //When
       double salary=employee.getSalary();
       //Then
       assertTrue(salary==2000);
   }
   @Test
   public void givenHourEmployee_whenRequestSalary_thenWillRturned(){
       //Given
       Employee employee=new HourEmployee("Employee",2,5);
       //When
       double salary=employee.getSalary();
       //Then
       assertTrue(salary==1000);
   }
}
