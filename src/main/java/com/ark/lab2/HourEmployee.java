/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab2;

public class HourEmployee extends Employee {
    private int hours;
    HourEmployee(String name,int yearExperience,int hours) {
        super(name,yearExperience);
        this.hours=hours;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    @Override
    public double getSalary() {
        return 0.0;
    }
    
}
