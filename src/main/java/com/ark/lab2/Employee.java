/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab2;

/**
 *
 * @author ahmed
 */
public abstract class Employee {
    private String name;
    private int yearExperience;

    public Employee(String name, int yearExperience) {
        this.name = name;
        this.yearExperience = yearExperience;
    }
    public abstract double getSalary();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearExperience() {
        return yearExperience;
    }

    public void setYearExperience(int yearExperience) {
        this.yearExperience = yearExperience;
    }
    
}
