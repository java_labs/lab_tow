/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab2;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ahmed
 */
public enum Salaries {
    MONTHLY_SALARY,HOUR_COST;
    private  Map<Integer,Double> monthlySalaryExperience =new HashMap<>();
    private  Map<Integer,Double> hourCostExperience =new HashMap<>();
    public Double getSalary(int yearExperience){
        setSalaryMap();
        return monthlySalaryExperience.get(yearExperience);
    }
    public Double getCost(int yearExperience){
        setHourMap();
        return hourCostExperience.get(yearExperience);
    }
    private void setSalaryMap(){
        monthlySalaryExperience.put(1,1000d);
        monthlySalaryExperience.put(2,2000d);
        monthlySalaryExperience.put(3,3000d);
    }
    
    private void setHourMap(){
        hourCostExperience.put(1,100d);
        hourCostExperience.put(2,200d);
        hourCostExperience.put(3,300d);
    }
    
}
